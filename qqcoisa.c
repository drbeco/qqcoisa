/***************************************************************************
 *   qqcoisa.c                                Version 20220917.015959      *
 *                                                                         *
 *   Criar uma historia com estrutura gramatical definida                  *
 *   Copyright (C) 2017-2022    by Ruben Carlo Benante                     *
 ***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License.               *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 *   To contact the author, please write to:                               *
 *   Ruben Carlo Benante                                                   *
 *   Email: rcb@beco.cc                                                    *
 *   Webpage: http://www.beco.cc                                           *
 *   Phone: +55 (81) 3184-7555                                             *
 ***************************************************************************/

/* ---------------------------------------------------------------------- */
/* includes, defines e structs */

#include <stdio.h> /* Standard I/O functions */
#include <stdlib.h> /* Miscellaneous functions (rand, malloc, srand)*/
#include <string.h> /* Strings functions definitions */

#define MAXD 80 /* Tamanho maximo de cada string */
#define MAXNM 2 /* Nomes masculinos */
#define MAXNF 2 /* Nomes femininos */
#define MAXSM 4 /* Substantivos masculinos */
#define MAXSF 4 /* Substantivos femininos */
#define MAXAM 4 /* Adjetivos masculinos */
#define MAXAF 4 /* Adjetivos femininos */
#define NHIST 0 /* Numero da ultima historia no menu */

struct dados
{
    char nm[MAXNM][MAXD];
    char nf[MAXNF][MAXD];
    char sm[MAXSM][MAXD];
    char sf[MAXSF][MAXD];
    char am[MAXAM][MAXD];
    char af[MAXAF][MAXD];
};

/* ---------------------------------------------------------------------- */
/* Prototipos */
void hist00(struct dados d); /* Ruben C Benante (exemplo) */

/* Insira acima o seu prototipo, com um comentario com seu nome */

/* ---------------------------------------------------------------------- */
/* Main function:
 * Input: none
 * Output: EXIT_SUCCESS
 * Process: get keywords and choose from a menu which story to read
 */
int main(void)
{
    int opt, i;
    struct dados d;
    char *p;

    printf("Programa Qualquer Coisa - Criador de historias\n\n");
    printf("Digite as seguintes palavras:\n");

    /* Nomes ---- */
    printf("%d nomes (masculinos)\n", MAXNM);
    for(i=0; i<MAXNM; i++)
    {
        printf("Nome %d\n", i+1);
        fgets(d.nm[i], MAXD, stdin);
        if((p=strchr(d.nm[i], '\n'))) *p='\0';
    }
    /* complete o programa pedindo:
     *  - nomes femininos em d.nf[]
     *  - substantivos masculinos em d.sm[] e femininos em d.sf[]
     *  - adjetivos masculinos em d.am[] e femininos em d.am[]
     */

    /* Menu com estorias. Adicione o seu titulo na ordem. */
    do
    {
        printf("Menu de Estorias:\n");
        /* ----------- inicio do menu ----------- */ 
        /* Insira o seu item numerado no menu como no modelo: */
        printf("0- Historia do %s com a %s\n", d.nm[0], d.nf[0]);

        /* ----------- fim do menu ----------- */ 
        printf("Digite opcao: ");
        scanf("%d", &opt);
    }while(opt<0 || opt>NHIST);

    printf("\n--------\n");

    /* Chamando a estoria no switch */
    switch(opt)
    {
        /* adicionar o seu case N como no modelo: */
        case 0:
            hist00(d);
            break;

        /* fim dos cases */
        default:
            printf("\nObrigado por rodar o programa QQCOISA!\n");
            return 0;
    }
    printf("\n");

    return EXIT_SUCCESS;
}

/* ---------------------------------------------------------------------- */

/* Titulo: A historia do NM com a NF
 * Autor: Ruben Carlo Benante
 */
void hist00(struct dados d)
{
    printf("A historia do %s com a %s\n\n", d.nm[0], d.nf[0]);
    
    printf("O %s era muito amigo da %s, ate que\n", d.nm[0], d.nf[0]);
    printf("%s comeu o %s da %s %s.\n", d.nm[1], d.sm[0], d.af[1], d.nf[1]);
    return;
}

/* Adicionar sua funcao como no exemplo anterior,
 * contendo:
 *      - Comentario com Titulo e Autor
 *      - Prototipo / API identica: void histNN(struct dados d)
 *      - a historia com printf() apenas.
 *      - um return no final
 */

/* ---------------------------------------------------------------------------- */
/* vi: set ai cin et ts=4 sw=4 tw=0 wm=0 fo=croqltn : C config for Vim modeline */
/* Template by Dr. Beco <rcb at beco dot cc>  Version 20160714.153029           */

